package com.example.hw16

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.hw16.databinding.FragmentTitlePickerBinding
import java.lang.ClassCastException

class TitlePicker : DialogFragment() {
    lateinit var binding: FragmentTitlePickerBinding
    lateinit var mOnInputChanged: OnInputChanged
    interface OnInputChanged{
        fun sendTitle(str:String)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTitlePickerBinding.inflate(inflater)
        binding.editTextTime.setText(CurrentTitleAndDate.tmpString)
        binding.cancel2.setOnClickListener {
            dismiss()
        }

        binding.apply.setOnClickListener {
            CurrentTitleAndDate.tmpString = binding.editTextTime.text.toString()
            Toast.makeText(requireContext(), CurrentTitleAndDate.tmpString, Toast.LENGTH_LONG).show()
            mOnInputChanged.sendTitle(CurrentTitleAndDate.tmpString)
            dismiss()
        }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mOnInputChanged = targetFragment as OnInputChanged
        }catch(e: ClassCastException){

        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = TitlePicker()
    }
}
