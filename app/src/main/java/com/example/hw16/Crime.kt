package com.example.hw16

class Crime(private var title: String, private var date: String, var isSolved: Boolean) {
    fun getTitle():String{
        return title
    }
    fun getData():String{
        return date
    }
    fun setTitle(t:String){
        title = t
    }
    fun setDates(d:String){
        date = d
    }
    fun getIsSolved():String{
        return if (isSolved) "Closed" else "Opened"
    }
}
