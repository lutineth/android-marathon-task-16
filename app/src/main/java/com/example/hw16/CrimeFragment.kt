package com.example.hw16

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import com.example.hw16.databinding.FragmentCrimeBinding

class CrimeFragment : Fragment(), TitlePicker.OnInputChanged, DatePickerFragment.OnInputChanged {
    private val currentCrime:Crime = Crime("Crime", "01.01.1970", true)
    lateinit var binding: FragmentCrimeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCrimeBinding.inflate(inflater)
        setValues()
        return binding.root
    }

    private fun setValues() {
        binding.date.text = currentCrime.getData()
        binding.view.text = currentCrime.getTitle()
        binding.issolved.text = currentCrime.getIsSolved()
    }

    override fun sendTitle(str: String) {
        currentCrime.setTitle(CurrentTitleAndDate.tmpString)
        setValues()
    }

    override fun sendDate(str: String) {
        currentCrime.setDates(CurrentTitleAndDate.tmpDate)
        setValues()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.chTitle.setOnClickListener {
            val customDialog = TitlePicker()
            customDialog.setTargetFragment(this, 1)
            CurrentTitleAndDate.tmpString = currentCrime.getTitle()
            fragmentManager?.let { it1 -> customDialog.show(it1, "customDialog") }

        }

        binding.chDate.setOnClickListener {
            val customDialog = DatePickerFragment()
            CurrentTitleAndDate.tmpDate = currentCrime.getData()
            customDialog.setTargetFragment(this, 1)
            fragmentManager?.let { it1 -> customDialog.show(it1, "customDialog") }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = CrimeFragment()
    }
}
