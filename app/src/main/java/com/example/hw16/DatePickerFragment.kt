package com.example.hw16

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.example.hw16.databinding.FragmentDatePickerBinding
import java.lang.ClassCastException


class DatePickerFragment : DialogFragment() {
    lateinit var binding:FragmentDatePickerBinding
    lateinit var mOnInputChanged:OnInputChanged
    interface OnInputChanged{
        fun sendDate(str:String)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDatePickerBinding.inflate(inflater)

        binding.editTextDate.setText(CurrentTitleAndDate.tmpDate)
        binding.cancel.setOnClickListener {
            dismiss()
        }

        binding.applyButton.setOnClickListener {
            CurrentTitleAndDate.tmpDate = binding.editTextDate.text.toString()
            mOnInputChanged.sendDate(CurrentTitleAndDate.tmpDate)
            dismiss()
        }

        return binding.root
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mOnInputChanged = targetFragment as OnInputChanged
        }catch(e: ClassCastException){

        }
    }


    companion object {

        @JvmStatic
        fun newInstance() = DatePickerFragment()
    }
}
